<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    const RULES = [
        'name' => 'required'
    ];

    protected $fillable = ['name','twitter'];

    public function albuns(){
        return $this->hasMany('App\Models\Album');
    }
}
