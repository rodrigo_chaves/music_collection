<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Album extends Model
{
    const RULES = [
        'artist_id' => 'required',
        'name' => 'required'
    ];

    protected $table = 'albuns';

    protected $fillable = ['artist_id','name', 'year'];

    public function artist(){
        return $this->belongsTo('App\Models\Artist');
    }
}
