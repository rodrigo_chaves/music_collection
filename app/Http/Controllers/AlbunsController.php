<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Album;
use App\Models\Artist;
use Validator;

class AlbunsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $albuns = Album::latest()->paginate(10);
        return View('albuns.index')->with('albuns',$albuns);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $artists = Artist::all();
        return View('albuns.create')->with('artists', $artists);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), Album::RULES);

        if($validation->fails()){
            return redirect()->back()->with('errors',$validation->errors());
        }

        $album = Album::create([
            'artist_id' => $request->artist_id,
            'name' => $request->name,
            'year' => $request->has('year')? $request->year : null
        ]);

        return redirect()->route('albuns.index')->with('success','Album saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artists = Artist::all();
        $album = Album::find($id);
        return View('albuns.edit')
                ->with('artists', $artists)
                ->with('album', $album);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), Album::RULES);

        if($validation->fails()){
            return redirect()->back()->with('errors',$validation->errors());
        }

        $album = Album::find($id);
        $album->artist_id =  $request->artist_id;
        $album->name = $request->name;
        $album->year = $request->has('year')? $request->year : null;
        $album->save();

        return redirect()->route('albuns.index')->with('success','Album updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
