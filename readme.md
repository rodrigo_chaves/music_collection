
## Music Collection App

## Requirements

        Framework : Laravel 5.4
        PHP >= 5.6.4
        Mysql

## Instalation

        git clone git@bitbucket.org:rodrigo_chaves/music_collection.git
        composer update
        cp .env.example .env
        //configure database fields according to your local machine
        php artisan key:generate
        php artisan migrate
        php artisan serve

## Tests
        The code tests are in the '/tests' folder.
        run `./vendor/bin/phpunit` on terminal to run

