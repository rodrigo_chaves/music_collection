<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Models\User;

class AuthTest extends TestCase
{

    /**
     * A not logged user shouldnt access artists page
     */
    public function testShouldntLoadArtistsWithoutLogin(){
        $response = $this->get(route('artists.index'));

        $response->assertStatus(302);
    }

    /** 
     *  Only logged users can access artists list
     */
    public function testAccessArtistsList(){
        $user = User::first();

        $response = $this->actingAs($user)
                         ->get(route('artists.index'))
                         ->assertStatus(200);
    }

    /** Albuns */

    /**
    *  A not logged user shouldnt access albuns page
    */
    public function testShouldntLoadArtistAlbuns(){
        $response = $this->get(route('albuns.index'));

        $response->assertStatus(302);
    }

    /**
     * Only logged users can access albuns list
     */
    public function testAccessAlbunsList(){
        $user = User::first();
        $response = $this->actingAs($user)
                         ->get(route('albuns.index'))
                         ->assertStatus(200);
    }
}
