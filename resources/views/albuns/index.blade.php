@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="panel panel-default">
                <div class="panel-heading">Albuns <a href="{{route('albuns.create')}}" class="btn btn-primary">New</a></div>

                <div class="panel-body no-padding">
                    <table class="table table-responsive">
                        <thead>
                            <th>Artist</th>
                            <th>Name</th>
                            <th>Year</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach($albuns as $album)
                                <tr>
                                    <td>{{$album->artist->name}}</td>
                                    <td>{{$album->name}}</td>
                                    <td>{{$album->year}}</td>
                                    <td>
                                        <a href="{{route('albuns.edit',['album' => $album->id])}}" class="btn">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfooter>
                            {{$albuns->links()}}
                        </tfooter>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
