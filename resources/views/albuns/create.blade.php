@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="panel panel-default">
                <div class="panel-heading">New Album</div>

                <div class="panel-body no-padding">
                    <form class="form-horizontal" method="POST" action="{{ route('albuns.store') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('artist_id') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Artist</label>

                            <div class="col-md-6">
                                <select name="artist_id" class="form-control" required>
                                    @foreach($artists as $artist)
                                    <option value="{{$artist->id}}">{{$artist->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('artist_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('artist_id') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>
                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                    required autofocus>

                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('year') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Year</label>
                            <div class="col-md-6">
                                <input id="year" type="text" class="form-control" name="year" value="{{ old('year') }}"
                                    required autofocus>

                                @if ($errors->has('year'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('year') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection