@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            <div class="panel panel-default">
                <div class="panel-heading">Artists <a href="{{route('artists.create')}}" class="btn btn-primary">New</a></div>

                <div class="panel-body no-padding">
                    <table class="table table-responsive">
                        <thead>
                            <th>Name</th>
                            <th>Twitter</th>
                            <th>Albuns</th>
                            <th></th>
                        </thead>
                        <tbody>
                            @foreach($artists as $artist)
                                <tr>
                                    <td>{{$artist->name}}</td>
                                    <td>{{$artist->twitter}}</td>
                                    <td>{{$artist->albuns()->count()}}</td>
                                    <td>
                                        <a href="{{route('artists.edit',['artist' => $artist->id])}}" class="btn">Edit</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfooter>
                            {{$artists->links()}}
                        </tfooter>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
